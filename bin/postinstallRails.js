const fs = require('fs')
const path = require('path')

const sourceDir = path.resolve(__dirname,'../assets/GlobalNavContainer.js')
const destinationDir = path.resolve(process.env.INIT_CWD,'./app/javascript/components/GlobalNav.js')
fs.copyFileSync(sourceDir, destinationDir);
